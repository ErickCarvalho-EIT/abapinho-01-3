*----------------------------------------------------------------------*
***INCLUDE Z_pbo.
*----------------------------------------------------------------------*

MODULE janela_1000_pbo OUTPUT.

  IF g_custom_container IS INITIAL.

    SET PF-STATUS '1000'.

    CREATE OBJECT g_custom_container EXPORTING container_name = g_container.
    CREATE OBJECT g_grid EXPORTING i_parent = g_custom_container.

    PERFORM prepare_fieldcatalog.

    CALL METHOD g_grid->set_table_for_first_display
      EXPORTING
        is_layout       = gs_layout
      CHANGING
        it_fieldcatalog = gs_fieldcatalog
        it_outtab       = gt_spfli.
  ELSE.
    CALL METHOD g_grid->refresh_table_display.
  ENDIF.

ENDMODULE.