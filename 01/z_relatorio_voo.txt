REPORT z_relatorio_voo.

DATA: estrutura_voo TYPE sflight.

SELECTION-SCREEN BEGIN OF BLOCK block WITH FRAME.
SELECT-OPTIONS:
  p_carrid FOR sflight-carrid NO INTERVALS,
  p_price  FOR sflight-price NO INTERVALS,
  p_fldate FOR sflight-fldate.
SELECTION-SCREEN END OF BLOCK block.


START-OF-SELECTION .

  SELECT * FROM sflight INTO  estrutura_voo WHERE carrid IN p_carrid
                                                  AND price IN p_price
                                                  AND fldate IN p_fldate.
    NEW-LINE .
    WRITE:
      estrutura_voo-carrid,
      estrutura_voo-connid,
      estrutura_voo-fldate .
  ENDSELECT .

  IF sy-subrc = 0.
    MESSAGE 'Retornado sy-dbindex registros com sucesso' TYPE 'S' .
  ELSE .
    MESSAGE 'Erro ao selecionar dados' TYPE 'E' .
  ENDIF.